import os

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

# VARS
DEBUG = os.getenv('DEBUG') == 'True'
DATABASE_URL = os.getenv('DATABASE_URL')
