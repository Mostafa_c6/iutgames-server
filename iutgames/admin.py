from flask_peewee.admin import Admin, ModelAdmin
from iutgames.app import app
from iutgames.auth import auth
from iutgames.database.models import Customer, Order, Timeslot, Event, OrderLog
from flask import abort
import os

admin = Admin(app, auth,
              prefix='/{admin}'.format(admin=os.getenv('ADMIN_PREFIX', 'lfva0g8DR75NgvBxhVmqSgjHOc')),
              branding='games')


class NoDeleteMixin(ModelAdmin):
    def delete(self):
        abort(403)


class NoAddMixin(ModelAdmin):
    def add(self):
        abort(403)


class NoEditMixin(ModelAdmin):
    def edit(self, pk):
        abort(403)


class OnlyViewModelAdmin(NoAddMixin,
                         NoDeleteMixin,
                         NoEditMixin):
    pass


class CustomerAdmin(OnlyViewModelAdmin):
    columns = ('name', 'student_id', 'created')


class OrderAdmin(OnlyViewModelAdmin):
    columns = ('uuid', 'trans_id', 'valid', 'finished', 'timeslot', 'customer')


class TimeslotAdmin(NoDeleteMixin):
    columns = ('id', 'event', 'day', 'minutes')


class EventAdmin(NoDeleteMixin):
    columns = ('name', 'price')


# class OrderLogAdmin(OnlyViewModelAdmin):
#     columns = ('action', 'dt')


auth.register_admin(admin)
admin.unregister(auth.User)
admin.register(Customer, CustomerAdmin)
admin.register(Order, OrderAdmin)
admin.register(Timeslot, TimeslotAdmin)
admin.register(Event, EventAdmin)
# admin.register(OrderLog, OrderAdmin)
