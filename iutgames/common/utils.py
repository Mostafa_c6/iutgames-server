from requests import post
import os
import arrow
import shortuuid
from munch import Munch

VERIFY_URL = 'https://www.google.com/recaptcha/api/siteverify'


def verify_recaptcha(response):
    try:
        res = post(VERIFY_URL,
                   dict(secret=os.getenv('RECAPTCHA_KEY'),
                        response=response))

        return res.json().get('success', False)
    except Exception as e:
        print(e)
        #  log error
        return False


locales = Munch(
    en=Munch(days=['Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'], free='free',
             undecided="undecided")
    , fa=Munch(days=['شنبه', 'یکشنبه', 'دوشنبه', 'سه شنبه', 'چهارشنبه', 'پنج شنبه', 'جمعه'], free='آزاد',
               undecided="نامشخص")
)


def minutes_formatting(index: int, locale: str = 'en') -> str:
    """
    >>> minutes_formatting(-1, 'en')
    'free'
    >>> minutes_formatting(-1, 'fa')
    'آزاد'
    >>> minutes_formatting(0)
    '17:00'
    >>> minutes_formatting(5)
    '18:40'
    >>> minutes_formatting(12)
    '21:00'
    """
    if index == -1:
        return locales.get(locale).free

    assert index <= 15
    delta = 20  # minutes_formatting
    base_time = arrow.get('2018-05-12T17:00:00+04:30')
    return base_time.shift(minutes=index * delta).format('HH:mm')


def day_formatting(day: int, locale: str = 'en') -> str:
    """
    >>> day_formatting(2, 'en')
    'Monday'
    >>> day_formatting(3)
    'Tuesday'
    >>> day_formatting(2 , 'fa')
    'دوشنبه'
    >>> day_formatting(-1, 'en')
    'free'
    >>> day_formatting(-1, 'fa')
    'آزاد'
    >>> day_formatting(5, 'fr')
    'undecided'
    """

    lang = locales.get(locale)
    if day == -1:
        return lang.free

    assert day < 7
    if locale == 'en':
        return lang.days[day]
    elif locale == 'fa':
        return lang.days[day]
    return locales.en.undecided


def gen_uuid():
    return shortuuid.uuid().lower()[:7]


if __name__ == '__main__':
    import doctest

    doctest.testmod()
