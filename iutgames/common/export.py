import csv
from iutgames.app import db


def get_valid_orders_cursor():
    return db.database.execute_sql('''
            select
              o.uuid as order_uuid,
              c.name,
              c.student_id,
              c.gender,
              c.phone,
              c.created
            from "order" o
              join timeslot t on o.timeslot_id = t.id
              join event e on t.event_id = e.id
              join customer c on o.customer_id = c.id
            where e.name = 'aria'
              and o.valid = TRUE
              and o.finished = TRUE
            order by
             c.created''')


def write_valid_orders_to_csv(file_name):
    cursor = get_valid_orders_cursor()
    with open(file_name, 'w') as f:
        writer = csv.writer(f)
        # write column names
        writer.writerow([i[0] for i in cursor.description])

        for row in cursor:
            writer.writerow(row)
