from requests import post
import os
from munch import munchify

GATEWAY_URL = 'http://api.nextpay.org/gateway/payment'

TOKEN_API = 'https://api.nextpay.org/gateway/token.http'
VERIFY_API = 'https://api.nextpay.org/gateway/verify.http'

CALLBACK_URI = os.getenv('CALLBACK_URI', 'https://httpbin.org/post')

API_KEY = os.getenv('API_KEY', 'dca13d28-c01c-43e9-8f45-25861f0c0efa')


def token_generation(order_id: str, amount: int):
    return munchify(post(TOKEN_API,
                         data=dict(api_key=API_KEY,
                                   order_id=order_id,
                                   amount=amount,
                                   callback_uri=CALLBACK_URI
                                   )).json())


def payment_verification(order_id: str, trans_id: str, amount: int):
    return munchify(post(VERIFY_API,
                         data=dict(
                             api_key=API_KEY,
                             order_id=order_id,
                             amount=amount,
                             trans_id=trans_id
                         )).json())


if __name__ == '__main__':
    tk = token_generation('test1', 102)
    if tk.get('code') == -1:
        print('{0}/{1}'.format(GATEWAY_URL, tk.get('trans_id')))
