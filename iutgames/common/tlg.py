import telepot
import os
import arrow
from ..jobs.tasks import send_tlg_message

admin = os.getenv('ADMIN_ID')
dev = '98624394'


def send_tlg_verified_order(order):
    message = """```
game       : {game},
name       : {name},
student ID : {std},
phone      : {phone},
order      : {code},
paid       : {paid} IRT,
when       : {when} ✅
```
        """.format(
        game=order.timeslot.event.name,
        code=order.uuid,
        name=order.customer.name,
        phone=order.customer.phone,
        std=order.customer.student_id,
        paid=order.timeslot.event.price,
        when=arrow.utcnow().to('Asia/Tehran').format('YYYY-MM-DD ddd HH:mm:ss'))

    send_tlg_message(message, admin)
    send_tlg_message(message, dev)


def send_tlg_verified_order_test():
    message = """```(test)
game       : {game},
name       : {name},
student ID : {std},
timeslot   : {timeslot},
phone      : {phone},
order      : {code},
paid       : {paid} IRT,
when       : {when} ✅
```
        """.format(
        game='حبابی',
        code='1e11e1',
        name='علی اکبری',
        timeslot='Sun 17:00',
        phone='91200000',
        std='9898989',
        paid=20000,
        when=arrow.utcnow().to('Asia/Tehran').format('YYYY-MM-DD ddd HH:mm:ss'))

    send_tlg_message(message, dev)


def send_tlg_dev_failed_order(order):
    message = """```
    (failed)
game       : {game},
name       : {name},
student ID : {std},
phone      : {phone},
order      : {code},
when       : {when} ❌
```
        """.format(
        game=order.timeslot.event.name,
        code=order.uuid,
        name=order.customer.name,
        phone=order.customer.phone,
        std=order.customer.student_id,
        when=arrow.utcnow().to('Asia/Tehran').format('YYYY-MM-DD ddd HH:mm:ss'))

    send_tlg_message(message, dev)
