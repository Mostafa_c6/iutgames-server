import logging

import pw_database_url
from flask import Flask
from flask_basicauth import BasicAuth
from flask_cors import CORS
from flask_peewee.db import Database
from flask_restful import Api

from iutgames.config import DEBUG, os

app = Flask(__name__)
api_origins = []
# todo : manage api origins
cors_rules = {r"^/api/*": {"origins":
                               ["http://localhost:3000",
                                "https://dev-iutaria.surge.sh",
                                "https://iutaria.tk"]},
              r"^/verify$": {"origins":
                                 "*"}}

cors = CORS(app, resources=cors_rules)
api = Api(app)

app.debug = DEBUG

key = os.getenv('FLASK_KEY')
if not key:
    raise Exception("app key is not specified")
app.secret_key = key

DATABASE = {
    'name': './example.db',
    'engine': 'peewee.SqliteDatabase',
}
if os.getenv('DATABASE_URL'):
    DATABASE = pw_database_url.parse(os.getenv('DATABASE_URL'))

app.config['DATABASE'] = DATABASE
db = Database(app)


def peewee_log():
    logger = logging.getLogger('peewee')
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())


if DEBUG:
    peewee_log()

app.config['BASIC_AUTH_USERNAME'] = os.environ['BASIC_AUTH_USERNAME']
app.config['BASIC_AUTH_PASSWORD'] = os.environ['BASIC_AUTH_PASSWORD']

basic_auth = BasicAuth(app)
