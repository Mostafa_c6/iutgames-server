from iutgames.app import app
from flask import render_template, request, redirect
from iutgames.common.payment import payment_verification
from flask import abort
from iutgames.database.models import Order, OrderLog, Event
from iutgames.common.tlg import send_tlg_verified_order, send_tlg_verified_order_test, send_tlg_dev_failed_order
from iutgames.config import DEBUG
import jdatetime
from munch import Munch
import locale

if DEBUG:
    @app.route('/test/verify')
    def verify_test():
        locale.setlocale(locale.LC_ALL, "fa_IR")

        data = dict(
            name='محمد عباسی',
            uuid='abc3cv',
            game=Event.get(name="abrang").display_name,
            day=jdatetime.datetime(1397, 2, 26).strftime('%A'),
            time='17:40'
        )
        return render_template('verify.html', data=data)


    @app.route('/test/verify/tlg')
    def tlg_test():
        send_tlg_verified_order_test()
        return 'okay'


    @app.route('/test/pdb')
    def pdb_test():
        o = Order.get()
        # print_db(o)
        return 'okay'


@app.route('/verify', methods=['POST'])
def verify():
    trans_id = request.values.get('trans_id')
    order_id = request.values.get('order_id')
    if not (trans_id and order_id):
        abort(400)
    order = Order.get_or_none(trans_id=trans_id, uuid=order_id)
    # todo : logger
    print(trans_id, order_id)

    if not order:
        return render_template('error.html',
                               error='invalid trans/order')
    if order.finished:
        return render_template('error.html',
                               error='your purchase is already done')

    # send for NextPay to verify

    res = payment_verification(
        order_id=order_id,
        trans_id=trans_id,
        amount=order.timeslot.event.price)

    res = Munch(res)

    if res.code == 0:
        # SUCCEEDED
        order.finishize()

        OrderLog.create(order=order,
                        action=OrderLog.Action.SUCCEEDED,
                        code=res.code)
        try:
            send_tlg_verified_order(order)
        except Exception as e:
            # todo : logger
            print('telegram error :', e)

        data = dict(
            name=order.customer.name,
            uuid=order_id,
            game=order.timeslot.event.display_name,
            day=order.timeslot.formatted_day('fa'),
            time=order.timeslot.formatted_minutes('fa')
        )
        return render_template('verify.html', data=data)

    else:
        # payment error, not valid anymore
        # FAILED

        order.invalidate()
        OrderLog.create(order=order,
                        action=OrderLog.Action.FAILED,
                        code=res.code)

        send_tlg_dev_failed_order(order)

        return render_template('error.html',
                               error='failure code ' + str(res.code),
                               uuid=order.uuid)
