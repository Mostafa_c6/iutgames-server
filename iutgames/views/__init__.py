from .verify import *
from .export import export


@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    # keep it for heroku script
    data = dict(name=name)
    return render_template('hello.html', data=data)


@app.route('/')
def index():
    return redirect('https://iutaria.tk')
