from iutgames.app import app, basic_auth
import os
import time
from iutgames.common.export import write_valid_orders_to_csv
from flask import Response

route = os.environ['EXPORT_ROUTE']


@app.route('/{}/export'.format(route), methods=['GET'])
@basic_auth.required
def export():
    time_str = str(int(time.time()))
    temp_path = '/tmp/output.csv'
    write_valid_orders_to_csv(temp_path)
    with open(temp_path, 'r') as fw:
        csv = fw.read()

    return Response(
        csv,
        mimetype="text/csv",
        headers={
            "Content-disposition":
                "attachment; filename=export-{timestamp}.csv".format(timestamp=time_str)
        }
    )
