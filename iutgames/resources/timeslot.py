from flask_restful import Resource, reqparse, inputs, abort
from munch import munchify

from ..database.models import Timeslot, Event

parser = reqparse.RequestParser()
parser.add_argument('event', type=inputs.regex('^\w{2,20}$'),
                    help="invalid event name",
                    required=True)


class TimeslotResource(Resource):
    def get(self):
        args = munchify(parser.parse_args())

        event = Event.get_or_none(name=args.event)
        if not event:
            abort(400, message='invalid event')

        res = list()
        for ts in Timeslot.ts_with_valid_orders_pf(event):
            avail = ts.capacity - len(ts.orders)
            res.append({'d': ts.day, 't': ts.minutes, 'avail': avail})

        return res
