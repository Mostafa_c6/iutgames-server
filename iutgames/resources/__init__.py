from iutgames.app import api
from .ping import Ping
from .timeslot import TimeslotResource
from .register import Register

api.add_resource(Ping, '/api/ping')
api.add_resource(TimeslotResource, '/api/timeslots')
api.add_resource(Register, '/api/register')
