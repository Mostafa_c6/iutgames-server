import os

import arrow
from flask_restful import Resource, reqparse, inputs, abort
from munch import munchify
from iutgames.database.models import Customer, Order, Timeslot, Event, OrderLog
from ..common.utils import verify_recaptcha
from ..common.utils import gen_uuid
from ..common.payment import GATEWAY_URL, token_generation
from ..common.tlg import dev
from ..jobs.tasks import send_tlg_message, check_validity_after_timeout
from peewee import IntegrityError

parser = reqparse.RequestParser(bundle_errors=True)

parser.add_argument('name', type=str, required=True)
# todo : max length regex
parser.add_argument('event', type=inputs.regex('^\w{2,20}$'), required=True,
                    help="invalid event name")
parser.add_argument('student_id', type=inputs.regex('^[0-9]{7}$'),
                    required=True, help='student_id must only contain 7 digits, starting with 8 or 9')
parser.add_argument('gender', type=inputs.regex('^[mf]$'), help="only 'm' or 'f' is accepted", required=True)

parser.add_argument('day', type=inputs.int_range(0, 5), required=True)
parser.add_argument('minutes', type=inputs.int_range(0, 15), required=True)

parser.add_argument('recaptcha', type=str, required=True)
parser.add_argument('phone', type=inputs.regex('^\d{10}$'), help="10 digit phone format", required=True)


# todo: extract odd/even dayDEV

def soccer_day_logic(args):
    if (args.gender == 'm' and args.day % 2 != 0) or \
            (args.gender == 'f' and args.day % 2 == 0):
        abort(403, message="gender and day do not match for soccer")


def abrang_day_logic(args):
    if (args.gender == 'm' and args.day % 2 == 0) or \
            (args.gender == 'f' and args.day % 2 != 0):
        abort(403, message="gender and day do not match for abrang")


# reject unfinished orders after 12 mins
ORDER_DELAY = int(os.getenv('ORDER_DELAY', 12 * 60))
print(ORDER_DELAY)


def dev_debugg_message(args):
    message = """```
    (pending...)
    game       : {game},
    name       : {name},
    student ID : {std},
    day, min   : {day} , {min},
    phone      : {phone},
    recaptcha  : {recaptcha},
    when       : {when} ❕❕
    ```
            """.format(
        game=args.event,
        name=args.name,
        day=args.day,
        min=args.minutes,
        phone=args.phone,
        std=args.student_id,
        recaptcha=args.recaptcha[:1],
        when=arrow.utcnow().to('Asia/Tehran').format('YYYY-MM-DD ddd HH:mm:ss'))
    return message


class Register(Resource):

    def post(self):
        args = munchify(parser.parse_args())

        message = dev_debugg_message(args)

        send_tlg_message(message, dev)
        # first and foremost recaptcha!
        # todo : you keep this on
        # for the stupid filtering in iran
        # need to keep it down
        # if args.recaptcha != 'Sd_C-LzBLH49XrmJcpEzl2Vj0H4':
        if not verify_recaptcha(args.recaptcha):
            abort(403, message='recaptcha failed')

        customer = Customer.create(name=args.name, student_id=args.student_id,
                                   gender=args.gender, phone=args.phone)

        event = Event.get(name=args.event)

        if event.name == 'soccer':
            soccer_day_logic(args)

        elif event.name == 'abrang':
            abrang_day_logic(args)

        # todo: optimize with prefetch
        ts = Timeslot.get((Timeslot.day == args.day)
                          & (Timeslot.minutes == args.minutes)
                          & (Timeslot.event == event))

        # rare case when, in mid time someone tried sooner
        if ts.full():
            abort(400, message="timeslot just got requested "
                               "try in 5 min for the same timeslot!")
        try:
            # the rare case where gen_uuid()
            # fails to produce not unique uuid
            order = Order.create(uuid=gen_uuid(), customer=customer,
                                 timeslot=ts)
        except IntegrityError as _:
            # do it again
            order = Order.create(uuid=gen_uuid(), customer=customer,
                                 timeslot=ts)

        check_validity_after_timeout.schedule(args=(order.id,), delay=ORDER_DELAY)

        OrderLog.create(order=order, action=OrderLog.Action.CREATED)

        token = token_generation(order.uuid, ts.event.price)

        OrderLog.create(order=order, action=OrderLog.Action.TOKEN, code=token.code)

        if token.code == -1:
            # save the related trans_id for verification
            order.trans_id = token.trans_id
            order.save()
            return {'redirect': '{gateway}/{trans_id}'.
                format(gateway=GATEWAY_URL, trans_id=token.trans_id)}, 200
        else:
            send_tlg_message('token error ' + token.code, dev)
            order.invalidate()
            abort(400, message="token error")
