from playhouse.migrate import *
from iutgames.app import db
from peewee import *

if 'lite' in db.database_engine:
    migrator = SqliteMigrator(db.database)
else:
    migrator = PostgresqlMigrator(db.database)

gender_field = CharField(default='e')


def run():
    with db.database.transaction():
        migrate(
            migrator.add_column('customer', 'gender', gender_field)
        )
