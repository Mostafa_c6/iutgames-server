from iutgames.app import db
from iutgames.database.models import *
import os

SOCCER = 'فوتبال حبابی'
ABRANG = 'آب و رنگ'
ARIA = 'جشن آریا'
REALLY_HIGH_CAP = 5000


def init(drop):
    if drop:
        drop_tables_2()
    create_tables()
    soccer = Event.create(name='soccer',
                          display_name=SOCCER,
                          price=os.getenv('SOCCER_PRICE', 20000))
    abrang = Event.create(name='abrang',
                          display_name=ABRANG,
                          price=os.getenv('ABRANG_PRICE', 10000))

    print(abrang.price)
    days = 5
    minutes = 15
    for i in range(days):
        for j in range(minutes):
            Timeslot.create(day=i, minutes=j, event=soccer, capacity=2)

    days = 5
    for i in range(days):
        # minutes=0 cuz come on i aint got time for this
        Timeslot.create(day=i, event=abrang, minutes=0, capacity=REALLY_HIGH_CAP)

    init_aria()


def init_aria():
    aria = Event.create(name='aria',
                        display_name=ARIA,
                        price=os.getenv('ARIA_PRICE', 13000))

    mnt = 12  # 21:00
    MEN_CAPACITY = os.getenv('MEN_ARIA_CAP', 305)
    WOMEN_CAPACITY = os.getenv('WOMEN_ARIA_CAP', 410)

    # for men(custom logic)
    Timeslot.create(day=2, minutes=mnt, event=aria, capacity=MEN_CAPACITY)
    # for women(custom logic)
    Timeslot.create(day=3, minutes=mnt, event=aria, capacity=WOMEN_CAPACITY)
