from peewee import *
import datetime
from ..common.utils import day_formatting,minutes_formatting
from iutgames.app import db
from enum import IntEnum


class Customer(db.Model):
    name = CharField(max_length=40)
    student_id = FixedCharField(max_length=7)
    phone = CharField(max_length=15)
    email = CharField(max_length=50, null=True)
    gender = CharField()

    created = DateTimeField(default=datetime.datetime.utcnow)

    def __unicode__(self):
        return '{std}:{name}'.format(std=self.student_id, name=self.name)


class Event(db.Model):
    name = CharField()
    display_name = CharField()
    price = IntegerField()  # in IRT (Iran Toman)
    note = TextField(null=True)

    def __unicode__(self):
        return '{name}, {price} IRT'.format(name=self.name, price=self.price)


class Timeslot(db.Model):
    day = IntegerField()  # day index
    minutes = IntegerField()  # minutes index, or null where no time shift is there
    event = ForeignKeyField(Event, related_name='timeslots', on_delete='CASCADE')
    capacity = IntegerField()

    def __unicode__(self):
        return '{day} {minutes}, cap: {cap}'.format(
            day=self.formatted_day(),
            minutes=self.formatted_minutes(),
            cap=self.capacity
        )

    def __str__(self):
        return self.__unicode__()

    def valid_orders_count(self):
        return self.orders.select().where(Order.valid).count()

    def full(self):
        return self.capacity == self.valid_orders_count()

    @staticmethod
    def ts_with_valid_orders_pf(event):
        ts = Timeslot.select().join(Event). \
            where(Timeslot.event == event)
        orders = (
            Order.select().where(Order.valid)
        )
        return prefetch(ts, orders)

    def formatted_day(self, locale='en'):
        return day_formatting(self.day, locale)

    def formatted_minutes(self, locale='en'):
        return minutes_formatting(self.minutes, locale)


class Order(db.Model):
    uuid = CharField(unique=True)
    customer = ForeignKeyField(Customer, related_name='orders', on_delete='CASCADE')
    timeslot = ForeignKeyField(Timeslot, related_name='orders', on_delete='CASCADE')
    trans_id = CharField(null=True)
    valid = BooleanField(default=True)
    finished = BooleanField(default=False)

    def __unicode__(self):
        return 'uuid:{uuid}, trans:{trans_id}, valid:{valid}, finished:{finished}'.format(
            uuid=self.uuid,
            trans_id=self.trans_id,
            valid=self.valid,
            finished=self.finished
        )

    def invalidate(self):
        self.valid = False

        print('{order} invalidated  for customer : {customer}'.format(
            order=self, customer=self.customer
        ))

        return self.save()

    def finishize(self):
        self.finished = True
        return self.save()


class OrderLog(db.Model):
    class Action(IntEnum):
        CREATED = 1
        TOKEN = 2
        SUCCEEDED = 3
        FAILED = 4

    order = ForeignKeyField(Order, related_name='order_logs', on_delete='CASCADE')
    action = SmallIntegerField()
    code = IntegerField(null=True)
    dt = DateTimeField(default=datetime.datetime.utcnow)

    def __unicode__(self):
        return 'action :{action} code:{code}, {dt}'.format(
            action=self.action,
            code=self.code,
            dt=self.dt
        )


def create_tables():
    Customer.create_table(),
    Event.create_table()
    Timeslot.create_table()
    Order.create_table()
    OrderLog.create_table()


def drop_tables_2():
    OrderLog.drop_table()
    Order.drop_table()
    Timeslot.drop_table()
    Event.drop_table()
    Customer.drop_table()

# c = Customer.create(fullname="mosi", student_id="12")
