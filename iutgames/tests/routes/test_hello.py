from main import app
from unittest import TestCase


class TestRoutes(TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_index_redirect(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 302)

    def test_hello_no_args(self):
        response = self.app.get('/hello/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(b'hello' in response.data)

    def test_hello_arg(self):
        name = "john"
        response = self.app.get('/hello/{}'.format(name))
        self.assertTrue(name.encode() in response.data)
