import base64
import os
from unittest import TestCase

from main import app


def make_basic_auth_header(_user, _pass):
    auth = base64.b64encode('{}:{}'.format(_user, _pass).encode())
    return {'Authorization': 'Basic {auth}'.format(auth=auth.decode())}


class TestRoutes(TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.route = os.getenv('EXPORT_ROUTE')

    def test_deny_on_export_get(self):
        response = self.app.get('/{}/export'.format(self.route))
        self.assertEqual(response.status_code, 401)

    def test_deny_on_other_methods(self):
        response = self.app.post('/{}/export'.format(self.route))
        self.assertEqual(response.status_code, 405)
        response = self.app.put('/{}/export'.format(self.route))
        self.assertEqual(response.status_code, 405)

    def test_deny_on_invalid_auth_credit(self):
        _user, _pass = 'wrong', 'auth'
        header = make_basic_auth_header(_user, _pass)
        response = self.app.get('/{}/export'.format(self.route),
                                headers=header)
        self.assertEqual(response.status_code, 401)

    def test_accept_on_authenticated_get(self):
        _user = os.getenv('BASIC_AUTH_USERNAME')
        _pass = os.getenv('BASIC_AUTH_PASSWORD')
        header = make_basic_auth_header(_user, _pass)
        response = self.app.get('/{}/export'.format(self.route),
                                headers=header)
        self.assertEqual(response.status_code, 200)
