from main import app
from unittest import TestCase


class TestRoutes(TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_ping_pong(self):
        response = self.app.get('api/ping')
        self.assertEqual(response.status_code, 200)

    def test_empty_get(self):
        path = '/api/timeslots'
        response = self.app.get(path)
        self.assertEqual(response.status_code, 400)

    def test_invalid_event_name(self):
        path = '/api/timeslots'
        response = self.app.get(path, data=dict(event='invalid/event/path'))
        self.assertEqual(response.status_code, 400)

    def test_invalid_register_names(self):
        event = "/clever/name"
        res = self.app.post('/api/register', data=dict(event=event))
        self.assertEqual(res.status_code, 400)
