from unittest import TestCase
from functools import wraps
from iutgames.database.models import Order, Timeslot, Event, Customer
from peewee import SqliteDatabase
from peewee import fn as pfn, Query

test_db = SqliteDatabase(':memory:')
MODELS = (Order, Timeslot, Event, Customer)


# Bind the given models to the db for the duration of wrapped block.
def use_test_database(fn):
    @wraps(fn)
    def inner(self):
        with test_db.bind_ctx(MODELS):
            test_db.create_tables(MODELS)
            try:
                fn(self)
            finally:
                test_db.drop_tables(MODELS)

    return inner


class TestUsersTweets(TestCase):

    @use_test_database
    def test_contains(self):
        # self.set_up()
        e1 = Event.create(name="test 1", price=300)
        e2 = Event.create(name="test 2", price=400)
        self.assertEqual(2, Event.select().count())

    @use_test_database
    def test_ts_orders(self):
        e1 = Event.create(name="test 1", price=300)
        e2 = Event.create(name="test 2", price=400)
        for i in range(6):
            Timeslot.create(day=i, minutes=i + 3, event=e1)
        for i in range(3):
            Timeslot.create(day=i, minutes=2 * i, event=e2)

        ts1, ts11 = Timeslot.select().where(Timeslot.event == e1).execute()[:2]
        ts2, ts22 = Timeslot.select().where(Timeslot.event == e2).execute()[:2]

        for i in range(6):
            c = Customer.create(name=str(i) + " user one", student_id=i, gender="m")
            Order.create(uuid=i, customer=c, timeslot=ts1)

        for i in range(6):
            c = Customer.create(name=str(i) + " user one", student_id=i, gender="m")
            Order.create(uuid=i, customer=c, timeslot=ts11)

        o2 = Order.get_by_id(2)
        o2.valid = False
        o2.save()

        o2 = Order.get_by_id(3)
        o2.valid = False
        o2.save()

        o5 = Order.get_by_id(5)
        o5.valid = False
        o5.save()

        Order.get_by_id(5)
        for i in range(6):
            c = Customer.create(name=str(i) + " user two", student_id=i, gender="m")
            Order.create(uuid=12 * i, customer=c, timeslot=ts2)

        o15 = Order.get_by_id(15)
        o15.valid = False
        o15.save()
        # print(list(Order))
        import logging
        logger = logging.getLogger('peewee')
        logger.setLevel(logging.DEBUG)
        logger.addHandler(logging.StreamHandler())
        ords = (Order.select()
            .join(Timeslot)
            .join(Event)
            .where(Event.id == e1.id, Order.valid == True)). \
            prefetch(Customer, Timeslot)

        self.assertEqual(len(ords), 9)
        for o in ords:
            print(o.uuid, o.customer.name, o.customer.student_id, o.timeslot)
zh5bfcp