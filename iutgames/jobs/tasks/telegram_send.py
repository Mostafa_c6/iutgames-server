import telepot
import os
from ..config import huey

bot = telepot.Bot(token=os.getenv('TELEGRAM_TOKEN'))

admin = os.getenv('ADMIN_ID')
dev = '98624394'


@huey.task(retries=3, retry_delay=10)
def send_tlg_message(text, who):
    bot.sendMessage(chat_id=who, text=text, parse_mode='Markdown')
