from ..config import huey
from iutgames.database.models import Order


@huey.task()
def check_validity_after_timeout(order_id):
    # must pass ID instead of instance!
    # when bank pending times out and yet no
    # answer, then cancel it
    order = Order.get_by_id(order_id)
    assert order.trans_id
    print(order)
    if not order.finished and order.valid:
        order.invalidate()
