from huey import RedisHuey
import os

redis_url = os.getenv('REDISTOGO_URL', 'redis://localhost:6379')
huey = RedisHuey('job-queue', url=redis_url)
