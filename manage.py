# manage.py
from getpass import getpass

from flask_script import Manager

from iutgames.app import app
from iutgames.auth import auth
from iutgames.common.export import write_valid_orders_to_csv
from iutgames.database.models import drop_tables_2
from iutgames.database.init_db import init, init_aria

manager = Manager(app)


@manager.command
def drop_db():
    drop_tables_2()


@manager.command
def init_db(drop=False):
    init(drop)


@manager.command
def init_admin():
    admin_db()
    admin_user()


@manager.command
def admin_db():
    auth.User.create_table(safe=True)


@manager.command
def admin_user():
    username = input('username :').strip()
    password = getpass('pass(hidden) :')

    admin = auth.User(username=username, email=username + '@site.com', admin=True, active=True)
    admin.set_password(password)
    admin.save()
    print('Admin user <{user}> created successfully.'.format(user=username))


@manager.command
def drop_admin_db():
    auth.User.drop_table()


@manager.command
def insert_aria():
    init_aria()


@manager.command
def dump_valids():
    write_valid_orders_to_csv('/tmp/output.csv')


if __name__ == "__main__":
    manager.run()
